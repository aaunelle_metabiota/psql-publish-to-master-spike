--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.movies DROP CONSTRAINT movies_pkey;
ALTER TABLE public.movies ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.movies_id_seq;
DROP TABLE public.movies;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: andrew
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO andrew;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: andrew
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: movies; Type: TABLE; Schema: public; Owner: andrew
--

CREATE TABLE movies (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    director character varying(25) NOT NULL
);


ALTER TABLE movies OWNER TO andrew;

--
-- Name: movies_id_seq; Type: SEQUENCE; Schema: public; Owner: andrew
--

CREATE SEQUENCE movies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE movies_id_seq OWNER TO andrew;

--
-- Name: movies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: andrew
--

ALTER SEQUENCE movies_id_seq OWNED BY movies.id;


--
-- Name: movies id; Type: DEFAULT; Schema: public; Owner: andrew
--

ALTER TABLE ONLY movies ALTER COLUMN id SET DEFAULT nextval('movies_id_seq'::regclass);


--
-- Data for Name: movies; Type: TABLE DATA; Schema: public; Owner: andrew
--

COPY movies (id, name, director) FROM stdin;
1	Inception	Christopher Nolan
2	Gone Girl	David Fincher
4	Interstellar	Christopher Nolan
5	Girl with the Dragon Tattoo	David Fincher
\.


--
-- Name: movies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: andrew
--

SELECT pg_catalog.setval('movies_id_seq', 5, true);


--
-- Name: movies movies_pkey; Type: CONSTRAINT; Schema: public; Owner: andrew
--

ALTER TABLE ONLY movies
    ADD CONSTRAINT movies_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

