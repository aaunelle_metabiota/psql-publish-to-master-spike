#!/bin/bash
# Test the success case for syncing ptm-staging to ptm-master
set -euo pipefail
IFS=$'\n\t'

readonly DUMPFILE="ptm-staging-dump.sql"

main() {
    echo -e "\n===== Testing DB sync success ====="
    setup_dbs
    make_changes_to_staging
    sync_staging_to_master
    assert_sync_success
    cleanup_dbs
}

setup_dbs() {
    ./create_test_dbs.sh
}

cleanup_dbs() {
    ./drop_test_dbs.sh
}

make_changes_to_staging() {
    psql ptm-staging --echo-all --file="sql/staging_changes.sql"
}

sync_staging_to_master() {
    pg_dump --dbname=ptm-staging --clean --file="$DUMPFILE"
    psql --dbname=ptm-master --single-transaction < "$DUMPFILE"
}

assert_sync_success() {
    assert_eq 1 $(psql ptm-master --tuples-only --command "SELECT COUNT(*) FROM movies WHERE name = 'Interstellar'")
    assert_eq 0 $(psql ptm-master --tuples-only --command "SELECT COUNT(*) FROM movies WHERE director = 'Steven Soderbergh'")
    assert_eq 4 $(psql ptm-master --tuples-only --command "SELECT COUNT(*) FROM movies")
    echo -e "\nTEST SUCCESS: All assertions passed"
}

assert_eq() {
    local -r expected="$1"
    local -r actual="$2"
    if [ $expected -ne $actual ]; then
        echo "Assertion failed: $1 is not equal to $2"
    fi
}


# Always run the cleanup function, even if there is an error
trap cleanup_dbs EXIT
main
