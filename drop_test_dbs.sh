#!/bin/bash
# Drops the test databases ptm-staging and ptm-master
set -euo pipefail
IFS=$'\n\t'

echo -e "\n===== Dropping databases ====="
dropdb --echo --if-exist ptm-staging
dropdb --echo --if-exist ptm-master
