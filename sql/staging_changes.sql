INSERT INTO movies (name, director) VALUES ('Interstellar', 'Christopher Nolan');
INSERT INTO movies (name, director) VALUES ('Girl with the Dragon Tattoo', 'David Fincher');
DELETE FROM movies WHERE director = 'Steven Soderbergh';
