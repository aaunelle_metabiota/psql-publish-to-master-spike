CREATE TABLE movies (
    id serial PRIMARY KEY,
    name varchar (50) NOT NULL,
    director varchar (25) NOT NULL
);

INSERT INTO movies (name, director) VALUES ('Inception', 'Christopher Nolan');
INSERT INTO movies (name, director) VALUES ('Gone Girl', 'David Fincher');
INSERT INTO movies (name, director) VALUES ('Oceans 11', 'Steven Soderbergh');
