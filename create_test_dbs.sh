#!/bin/bash
# Creates the test databases ptm-staging and ptm-master
set -euo pipefail
IFS=$'\n\t'

# Create the dbs
echo -e "\n===== Creating databases ====="
createdb --echo ptm-staging
createdb --echo ptm-master

# Add table and some rows to each
echo -e "\n===== Adding some tables and data ====="
psql ptm-staging --echo-all --file="sql/setup_tables.sql"
psql ptm-master --echo-all --file="sql/setup_tables.sql"
